/**
 * restService.js
 * It starts restify server to return server imageData via GET, and it have
 * a POST to get information from ATG server (TODO define format that ATG will send)
 *
 * Author: Obed Murillo
 * Copyright 2014 Speed Commerce
 */

var restify = require('restify'),
    fs = require('fs'),
    bunyan = require('bunyan'),
    wait = require('wait.for'),
    mongodbManager = require('./mongodbManager');

var logger = bunyan.createLogger({name: "Speed-pix Rest API Server"});

/**
 * it returns the data on meida mongodb collection which is created after images are processed.
 * 
 * @param req
 * @param res
 * @param next
 */
function respondGet(req, res, next) {
    wait.launchFiber(getAllMediaFiber, res);
}

/**
 * fiber used to get all medias.
 * 
 * @param res
 * @param next
 */
function getAllMediaFiber(res, next) {
     var allMedia = wait.forMethod(mongodbManager, 'getAllMedia');
     res.send(allMedia);
     next();
}

/**
 * It will get a param called skuData if exist will save the info into a file with the timeStamp.
 * @param req
 * @param res
 * @param next
 */
 function respondPost(req, res, next) {
    if (req.params.skuData === undefined) {
         return next(new restify.InvalidArgumentError('skuData must be supplied'));
    }
    try{
        var skuData = JSON.parse(req.params.skuData);
         for(i in skuData.allProducts){
             mongodbManager.saveATGProduct(skuData.allProducts[i]);
         }
         res.send(201, "Sku Data has been saved into image Server.");
         return next();
     } catch(err) {
        var example = '{"allProducts":[{"productId" : "1234", "skus" : [{"skuId" : "213", "colorCode": "NoColor"},{"skuId" : "007", "colorCode": "Red"}]}]}';
        res.send(202, "Sku Data has no valid json format, send request as follows: " + example);
     }
 }
 
/**
 * Start the server at port 8888
 */
 exports.startService = function () {
     var server = restify.createServer();
     server.get('/getImageData', respondGet);
     server.use(restify.bodyParser());
     server.post('/importSkuData', respondPost);
     server.listen(8888, function() {
         logger.info("Running Rest API at Port: 8888");
     });
 }