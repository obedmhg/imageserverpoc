/**
 * mongodbManager, this will be a util to save, and remove
 * from mongodb using mongoose.
 * 
 * author: Obed Murillo
 */

var mongoose = require('mongoose'),
                wait = require('wait.for'),
                bunyan = require('bunyan');

/* mongoDB connection settigs are set here.*/
mongoose.connect('mongodb://localhost/test');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
    logger.info('MongoDB connection started...');
});

//Definition of atgProductData needed for image processing.
var atgProductsSchema = mongoose.Schema({
        productId:  { type: String, unique: true }, 
        skus: [ {skuId: String, colorCode: String} ],
        processed: Boolean
    });

//Definition of mediaData that will be used for ATG as media repositoryItem
var mediaSchema = mongoose.Schema({
        mediaId:  { type: String, unique: true },
        name: String,
        path: String,
        url: String
    });

//We model objects based on atgProducts and media shcemas.
var ATGProduct = mongoose.model('atgProducts', atgProductsSchema);
var Media = mongoose.model('media', mediaSchema);

var logger = bunyan.createLogger({name: "Speed-pix mongodbManager"});

/**
 * It will save an atgProduct into the mongoDB the product contains the id and the sku specification.
 * it will save a new if productId does not exist, and it will update if the id already exist.
 * 
 */
exports.saveATGProduct = function (jsonProduct) {
    var product = new ATGProduct({ 
            productId : jsonProduct.productId,
            skus : jsonProduct.skus,
            processed : false 
            }
        );
        product.save(function (err, product) {
            if (err) return logger.debug('Product already exist on mongodb, updating definition.');
                logger.debug(product.productId + ' has been saved into mongodb.');
        });    
}

/**
 * It will remove an atgProduct by productId.
 * 
 */
exports.removeATGProductById = function (id) {    
    ATGProduct.findOneAndRemove({ productId: id }, function (err, product) {
         if (err) throw err;
     });    
}

/**
 * It will save an media into the mongoDB.
 * it will save a new if meida does not exist, and it will update if the id already exist.
 * 
 */
exports.saveMedia = function (jsonMedia) {
    var media = new Media({
            mediaId: jsonMedia.mediaId,
            name: jsonMedia.name,
            path: jsonMedia.path,
            url: jsonMedia.url
        });
    media.save(function (err, media) {
            if (err) return logger.debug('Media alrady exist on db, updating definition.');
                logger.debug(media.mediaId + ' has been saved into mongodb.');
        });    
}

/**
 * It will remove an media by mediaId.
 * 
 */
exports.removeMediaById = function (id) {    
    Media.findOneAndRemove({ mediaId: id }, function (err, product) {
         if (err) throw err;
     });    
}

/**
 * It retusn a list of medias.
 * 
 */
exports.getAllMedia = function(){
    var response = JSON.parse('{"allMedia": []}');
    var medias = wait.forMethod(Media, 'find', {});
        medias.forEach(function(media) {
            var mediaJson = {mediaId: media.mediaId};
            response.allMedia.push(mediaJson);
        });
    return response;
}
