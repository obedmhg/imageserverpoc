/**
 * resizeProcessor.js
 * This Processor will be in charge of unzip the images file, and it will
 * do resize for all scales defined in config.json
 * 
 * Author: Obed Murillo
 * Copyright 2014 Speed Commerce
 */

var watch = fs = require('fs'),
    AdmZip = require('adm-zip'),
    wait = require('wait.for'),
    lwip = require('lwip'),
    mailTools = require('./mailTools'),
    mongodbManager = require('./mongodbManager'),
    atgXmlImporter = require('./atgXmlImporter'),
    bunyan = require('bunyan');
    require('shelljs/global');

var logger = bunyan.createLogger({name: 'Speed-pix Resize Processor', 
                                level: 'info', 
                                /*streams: [{path: './imageServer.log' }] */ 
                                });

/**
 * Main function that will check if the file to process is a zip, if so it will
 * unzip the file and move the contnet to a process directory where it will
 * grab the images to resize them.
 * If something goes wrong it will send an email based on the configurations that are under
 * config.json file.
 */
exports.processUploadedFile = function(file) {
    try {
        if (file.path.indexOf('.zip') > -1) {
            logger.info('Resize Processor has started.');
            logger.info(file.path + ' Will be processed.');
            var configFile = fs.readFileSync('./drop/config.json', 'utf-8');
            var config = JSON.parse(configFile);
            wait.launchFiber(unzipFile, file.path, config);
            var files = fs.readdirSync(config.processDirectory);
            wait.launchFiber(resizeImages, files, config);
        }
    } catch(err) {
        logger.info(err);
        mailTools.sendAlert(config);
    }     
}

/**
 * It get a list of files that will be resized, also gets config object to get scales to
 * resize.
 * If Something goes wrong it will save into imagesData.json as exception items.
 * 
 * @param files
 * @param config
 */
function resizeImages(files, config) {
    var imageData = '{"products":[]}';
    var imageDataJson = JSON.parse(imageData);
    for (var i in files) {
        try {
            wait.launchFiber(resize, files[i], config);
            var productInfoJson = getProductInfoFromImage(files[i]);
            imageDataJson['products'].push(productInfoJson);
        } catch (err) {
            var productInfoJson = getProductInfoFromImage(files[i]);
            imageDataJson['products'].push(productInfoJson);
        }
    }
    fs.appendFileSync('./drop/imagesData.json', JSON.stringify(imageDataJson));
}

/**
 * It gets an imageName and does the resize for all the scales
 * that were send in scales list.
 * @param imageName
 * @param scales
 */
function resize(imageName, config) {
    logger.debug('Resizing Image ' + imageName);
    for (var i in config.scales) {
        try{
            var image = wait.for(lwip.open, config.processDirectory + imageName);
            if (!fs.existsSync(config.targetDirectory + config.scales[i].name)) {
                mkdir(config.targetDirectory + config.scales[i].name); 
            }
            var resizeImage = wait.forMethod(image, 'resize', config.scales[i].width, config.scales[i].height);
            wait.forMethod(resizeImage,'writeFile', config.targetDirectory + config.scales[i].name + '/' +imageName);
            var productInfoJson = getProductInfoFromImage(imageName);
            var mediaJson = {
                mediaId: productInfoJson.mediaIdPrefix + '_' + config.scales[i].name,
                name: config.contentServerPath + config.scales[i].name + '/' +imageName,
                path: config.contentServerPath + config.scales[i].name,
                url: config.baseUrl + config.contentServerPath + config.scales[i].name + '/' +imageName
            };
            mongodbManager.saveMedia(mediaJson);
          } catch(err) {
              logger.error('Error Resizing image: ' + imageName + ': ' +err);
          }
      }
      
}

/**
 * It extracts product information from an imageName. and retursn that into a jsonObject as follows:
 * {
 *		productId: 709342342, 
 *		colorCode: 605, 
 *		atlCode: alt_1, 
 *		mediaIdPrefix: 709342342_605_alt1
 * }
 *
 */
function getProductInfoFromImage(imageName) {
    var productInfo = imageName.replace('.jpg', '').split('_');
    if (productInfo[2] != undefined) { 
        return {
                productId: productInfo[0], 
                colorCode: productInfo[1], 
                atlCode: productInfo[2], 
                mediaIdPrefix: productInfo[0] + '_' + productInfo[1] + '_' + productInfo[2]
            };
    } else {
        return {
                productId: productInfo[0], 
                colorCode: productInfo[1], 
                mediaIdPrefix: productInfo[0] + '_' + productInfo[1]
            };
    }
}

/**
 * Unzip a file into process directory.
 * TODO the file destination will be good if it is configurable along with the rest of the paths
 * @param filename
 * @returns
 */
function unzipFile(filename, config) {
    try{
        var zip = new AdmZip(filename);
        zip.extractAllTo(config.processDirectory, true);
    } catch (err) {
        logger.error('Error trying to unzip file ' + filename);
    }
}
