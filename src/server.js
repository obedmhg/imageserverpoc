/**
 * server.js
 * Speed-pix Server that will receive images in zip file format, unzip those and
 * process them into different sclaes, based on client specific settings,
 * also will expose a rest API to POST product data from ATG.
 *
 * Author: Obed Murillo
 * Copyright 2014 Speed Commerce
 */

var DesktopUploader = require('desktop-uploader').DesktopUploader,
    restService = require('./restService'),
    resizeProcessor = require('./resizeProcessor');
    bunyan = require('bunyan');
    require('shelljs/global');

var logger = bunyan.createLogger({name: 'Speed-pix Image Server', 
                                level: 'debug', 
                                /*streams: [{path: './imageServer.log' }] */ 
                                });

var uploader = new DesktopUploader({
                                     name: 'speed-pix',
                                     configPath: null,
                                     paths: ['./drop/'],
                                         retries: 2
                                    });
 
logger.info('Image Server has Started..');

//Event Handlers
uploader.on('upload', processUploadedFile);
 
uploader.on('error',
 function(err, filename) {
    logger.error(err  + '(filename: ', filename, ')');
 }
);
 
uploader.resume();
 
function processUploadedFile(file, done) {
    resizeProcessor.processUploadedFile(file); 
}

//Call to restService to start the restApi part
restService.startService();