/**
 * atgXmlImporter.js
 * This importer will be in charge of create the xml with
 * the media and the product info and import that into ATG with startSQLRepository.
 * this importer will be used mustlikly for ATG11.1 clients
 * 
 * Author: Obed Murillo
 * Copyright 2014 Speed Commerce
 */


var watch = fs = require('fs'),
    mongodbManager = require('./mongodbManager'),
    wait = require('wait.for'),
    xmlbuilder = require('xmlbuilder'),
    bunyan = require('bunyan');
    require('shelljs/global');

var logger = bunyan.createLogger({name: "Speed-pix atgXmlImport"});

exports.exportXml = function () {
    var allMedia = wait.launchFiber(getAllMediaFiber);
    logger.info(allMedia);
}


function getAllMediaFiber() {
    var allMedia = wait.for(mongodbManager, 'getAllMedia');
    return allMedia;
}
