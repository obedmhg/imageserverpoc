/**
 * mailTools.js
 * Image server main
 *
 * Author: Obed Murillo
 * Copyright 2014 Speed Commerce
 */

 var nodemailer = require('nodemailer'),
    // create reusable transporter object using SMTP transport
    transporter = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        user: 'obed.murillo.sfc@gmail.com',
        pass: 'Obed33@speed'
      }
    });

/**
 * It send the email with emailOptios as parameters.
 * @param mailOptions
 */
 function sendEmail (mailOptions) {
    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
      if(error){
        console.log(error);
      }else{
        console.log('Message sent: ' + info.response);
      }
    });

  }
  
  /**
   * It send an alert based, it gets recipients from config object.
   * (TODO) Alert Details.
   */
   exports.sendAlert = function (config) {
     if (config != undefined) {
      var mailOptions = {
            from: 'Image Server <imageServer@speedcommerce.com>', // sender address
            to: config.alertRecipient, // list of receivers
            subject: 'Something went Wrong with Image Processor.', // Subject line
            text: 'Something went Wrong with Image Processor, plese check server logs.', // plaintext body
            html: '<b>Something went Wrong with Image Processor.</b>' // html body
          };
        }
        sendEmail(mailOptions);
      }